# Welcome to the Best Practices workshop!
If you are looking to level up your engineering skillz, or refresh on what you already know, this is the place for you! The Best Practices workshop is a bi-weekly series of interactive lessons covering best practices in technical development. The goal of each lesson is to cover a specific topic that has an active learning component to it, and have participants submit their solution to the problem.

At the end of a pre-determined number of lessons, all submissions will be counted and put into a draw! All submissions before the beginning of the next lesson will count as 2 points towards the draw. Don't worry if you get really busy and can't submit, any submissions before the specified deadline will still count towards the draw.

Now at this point, you must be wondering what happens if you win the draw! That's a great question that I currently don't know the answer to, but will figure out hopefully before the submission deadline.

# Best Practice Topics

This is the list of topics to be covered in the Best Practices series. Only the first X (most likely 10) lessons will count towards the draw, all following lessons are supplementary.

### Topic 1: Good Naming Convention

Naming convention is not very technical, however it is a very important part of development. During this workshop, we will discuss the best practices for naming in code, and do live examples in different scenarios. The goal of this lesson is not to set in stone a singular way to name things in your code, but instead to set standards and use the context of the project for your naming.

Reference Material: [Clean Code by: Robert C. Martin](https://www.investigatii.md/uploads/resurse/Clean_Code.pdf)

### Topic 2: SOLID Design

Reference Material: [SOLID Kata practice examples.](https://kata-log.rocks/solid-principles)

### Topic 3: Comments in Code and Git Messages

### Topic 4: Reviewing Code and Getting Code Reviewed

### Topic 5: Error Handling

### Topic 6: Unit tests

### Topic 7: Concurrency

### BP X series: When to Refactor Code? With Special Guest - _

### BP X series: Choosing one Cloud over the Other? With Special Guest - _

### BP X series: Where to stop? A guide to good enough With Special Guest - _
