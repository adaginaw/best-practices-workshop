interface ITodo {
  theList: number [];
  theDayNumbers: number;
  longFirstTaskString: string;

  getThem(): number[];
}

class TodoList implements ITodo {
  
  public theList: number[] = [];
  public theDayNumbers: number = 4;
  public longFirstTaskString: string = "The first task task is long."

  /**
   * Pretend that this function represents the numbers of days it takes to do a task.
   * There is a special condition that happens if the first task takes 4 days to complete.
   * This function wants to get only the lists where the first task takes 4 days.
   */
  public getThem(): number[] {
    let list1: number[] = [];
    for (let x of this.theList) {
      if (x[0] === this.theDayNumbers) {
        list1.push(x);
        console.log(this.longFirstTaskString);
      }
    }
    return list1;
  }
}

