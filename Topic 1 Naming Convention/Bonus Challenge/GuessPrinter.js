/**
 * 
 * @param {Character} candidate 
 * @param {Integer} count
 * This function is supposed to print out a string of statistics.
 * If you want an example context, 
 * imagine that this function is printing out a string saying how many of a single letter appears in a word. 
 */
function launchGuessStatisticsFazer(candidate, count) {
  let number;
  let verb;
  let pluralModifier;

  if (count == 0) {
    number = "no";
    verb = "are";
    pluralModifier = "s";
  } else if (count == 1) {
    number = "1";
    verb = "is";
    pluralModifier = "";
  } else {
    number = "" + count;
    verb = "are";
    pluralModifier = "s";
  }
  let guessMessage = String.format(
    "There %s %s %s%s",
    verb,
    number,
    candidate,
    pluralModifier
  );
  console.log(guessMessage);
}
